# -*- coding: utf-8 -*-

import tabula
import pandas
from calendar_utils import get_calendar
from ical_utils import cal_to_ical
from dokeos import get_list, download_file
from config import groups

def pdf_to_pandas(file_name):
    df = [
        tabula.read_pdf(
            file_name,
            pages=1,
            lattice=True,
            area=[11.6, 0, 94, 100],
            relative_area=True)
    ]
    i = 2
    while True:
        try:
            df.append(
                tabula.read_pdf(
                    file_name,
                    pages=i,
                    lattice=True,
                    area=[0, 0, 94, 100],
                    relative_area=True))
            i += 1
        except Exception:
            break
    return pandas.concat(df, ignore_index=True)


        
urls = get_list()
dfs = []
for url in urls:
    file_name = download_file(url, './planning/')
    dfs.append(pdf_to_pandas(file_name))

df = pandas.concat(dfs, ignore_index=True)

for group in groups:
    ical = cal_to_ical(get_calendar(df, group))
    with open(group + '.ics', 'wb') as f:
        f.write(ical)
        f.close()
